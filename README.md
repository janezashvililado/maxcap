# Video capture script maxcap.sh

## Setting up and configure Jetson Nano board

Use official [getting started guide](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-2gb-devkit) 

Some important notes from guide:
- Recommended USB-C power supply (5V⎓3A)
- For system boot microSD card, the minimum requirement is a 32GB UHS-1 card.


## Install and run video capture script
The `maxcap.sh` can be run as a systemd service, for this you need to copy it into the /etc/systemd/system/ folder. 
In the systemd service file `ExecStart` parameter points to the maxcap.sh path. 

### systemd service file
```
[Unit]
Description=Continues capture camera sream to files
[Service]
Type=simple
ExecStart=/home/maxin/repos/maxcap/maxcap.sh
[Install]
WantedBy=multi-user.target
```

Start the service with:
``` bash
systemctl start maxcap.service
```

Stop the service with:
``` bash
systemctl stop maxcap.service
```

Get status of the service with:
``` bash
systemctl stop maxcap.service
```

Start the service automaticaly at reboot time:
``` bash
systemctl enable maxcap.service 
```
### Configuration parameters header 
`maxcop.sh` file can be edited directly to change some video capture parameters. 

The sample of the configuration parameter header
 ```bash
#!/bin/bash

# Warning don't make additional spaces, bash can't handle it!

# Directory path for video files
DIR_PATH=/media/wifisher/USB-DATA/

# Video recording duration in seconds
DURATION=5

# Video size in bytes
SIZE_BYTES=500000000

# Set recommended resolution and framerate acording to this table:
# width/height| Max FPS
#------------------------
# 3264 x 2464 | 21
# 3264 x 1848 | 21
# 1920 x 1080 | 30
# 1640 x 1232 | 30
# 1280 x 720  | 58
# 1280 x 720  | 116
WIDTH=1280
HEIGHT=720
FPS=15

# Coding options: OMX_H264, OMX_H265, V4L2_H264, V4L2_H265
CODING=OMX_H264

...
```

### Reference links
[Example Pipelines](https://developer.ridgerun.com/wiki/index.php?title=Jetson_Nano/GStreamer/Example_Pipelines)
