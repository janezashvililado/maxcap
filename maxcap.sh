#!/bin/bash

# Warning don't make additional spaces, bash can't handle it!

# Directory path for video files
DIR_PATH=/media/wifisher/USB-DATA/

# Video recording duration in seconds
DURATION=300

# Video size in bytes
SIZE_BYTES=500000000

# Set recommended resolution and framerate acording to this table:
# width/height| Max FPS
#------------------------
# 3264 x 2464 | 21
# 3264 x 1848 | 21
# 1920 x 1080 | 30
# 1640 x 1232 | 30
# 1280 x 720  | 58
# 1280 x 720  | 116
WIDTH=1280
HEIGHT=720
FPS=15

# Coding options: OMX_H264, OMX_H265, V4L2_H264, V4L2_H265
CODING=OMX_H264


if [[ $CODING = OMX_H264 ]]; then
    H_FORMAT="omxh264enc";
    EM="";
elif [[ $CODING = V4L2_H264 ]]; then
    H_FORMAT="nvv4l2h264enc bitrate=8000000 ! h264parse";
    EM="-e";
elif [[ $CODING = OMX_H265 ]]; then
    H_FORMAT="omxh265enc";
    EM="";
elif [[ $CODING = V4L2_H265 ]]; then
    H_FORMAT="nvv4l2h265enc bitrate=8000000 ! h265parse";
    EM="-e";
fi

sleep 5;
FP11=$(($(ls $DIR_PATH|grep last-file-name|cut -d'_' -f2)+1));
rm $DIR_PATH'last-file-name'*;

if [[ $FP11 != '' ]];
then
    LAST_FILE_NAME=$DIR_PATH'last-file-name_'$FP11;
    DIR_PATH=$DIR_PATH$FP11-%01d.mp4;
else
    LAST_FILE_NAME=$DIR_PATH'last-file-name_0'
    DIR_PATH=$DIR_PATH'video'%01d.mp4;
fi

echo '-------------------'
echo $DIR_PATH;
echo '-------------------'
touch $LAST_FILE_NAME;
gst-launch-1.0 $EM nvarguscamerasrc ! "video/x-raw(memory:NVMM), width=$WIDTH, height=$HEIGHT,format=NV12, framerate=$FPS/1" ! $H_FORMAT ! splitmuxsink location=$DIR_PATH max-size-time=$(($DURATION * 1000000000)) max-size-bytes=$SIZE_BYTES;
